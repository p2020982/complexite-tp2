#include "skip_liste.hpp"

#include <cassert>
#include <random>

int main() {
  
  { //insertion en s'assurant du tri
    SkipListe sl ;

    for(int i = 20; i >= 0; --i) {
      sl.inserer(i) ;
    }
    sl.afficher() ;

    assert(sl.test_tri()) ;
        
    //test de recherche
    assert(sl.chercher(10)!=nullptr);
    assert(sl.chercher(20)!=nullptr);
    assert(sl.chercher(1000)==nullptr);

    //ajout de niveau et test de insertion et recherche avec niveau
    sl.afficher();
    assert(sl.chercher(10)!=nullptr);
    sl.inserer(10);
    sl.afficher();
    for(int i = 21; i<30; i++) {
      sl.inserer(i);
    }
    sl.afficher();
  
  }

  { //insertion en n'assurant plus le tri
    SkipListe sl ;

    //generateurs aleatoires
    std::default_random_engine rd ;
    std::uniform_int_distribution<int> rand_int(0,99) ;

    for(int i = 80; i >= 0; --i) {
      sl.inserer(rand_int(rd)) ;
    }
    sl.afficher() ;

    assert(sl.test_tri()) ;
    
  }
  

}
