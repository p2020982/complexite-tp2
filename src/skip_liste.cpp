#include "skip_liste.hpp"

#include <iostream>

SkipListe::SkipListe() {
  /* la valeur est inutile, mais on met -2147483648 */
  m_sentinelle = new SkipCellule(1<<31) ;
}

SkipListe::~SkipListe() {
  /* destruction recursive via le destructeur de SkipCellule */
  delete m_sentinelle ;
}

void SkipListe::inserer(int v) {
  SkipCellule* nlle = new SkipCellule(v) ;

  /* cette insertion n'est pas triee, c'est votre travail de le faire
  nlle->suivante[0] = m_sentinelle->suivante[0] ;
  m_sentinelle->suivante[0] = nlle ;
  */
  /* pour trier : avancer dans la liste jusqu'a trouver une valeur plus grande
   * ou la fin de la liste, en gardant le curseur sur la precedente. Une fois arrive
   * sur la celule plus grande ou la fin de la liste, il faut inserer apres le
   * curseur (un peu comme au dessus pour inserer apres la sentinelle).
   *
   * initialiser un curseur sur la sentinelle
   * Tant que le curseur a une cellule suivante et que sa valeur est plus petite que v {
   *    avancer le curseur sur sa suivante
   * }
   * inserer apres le curseur
   *
   * 
  SkipCellule* cur1 = m_sentinelle;
  if(m_sentinelle->suivante.size()==2) {
    while(cur1->suivante[1] && cur1->suivante[1]->valeur < v) {
      cur1 = cur1->suivante[1];
    }
  }
  SkipCellule* cur0 = cur1;
  while(cur0->suivante[0] && cur0->suivante[0]->valeur < v) {
    cur0 = cur0->suivante[0];
  }
  nlle->suivante[0] = cur0->suivante[0];
  cur0->suivante[0] = nlle;
  if(m_sentinelle->suivante.size()==2 && pile_ou_face()) {
    std::cout << "Valeur choisie : " << v << std::endl;
    nlle->suivante.push_back(nullptr);
    nlle->suivante[1] = cur1->suivante[1];
    cur1->suivante[1] = nlle;
  }

*/

  int nv_max = m_sentinelle->suivante.size()-1;
  int nv_courant = nv_max;
  SkipCellule* precedentes[nv_max+1];
  SkipCellule* cur = m_sentinelle;
  while(nv_courant>=0) {
    while(cur->suivante[nv_courant] && cur->suivante[nv_courant]->valeur < v) {
      cur = cur->suivante[nv_courant];
    }
    precedentes[nv_courant] = cur;
    nv_courant--;
  }
  nv_courant = 0;
  nlle->suivante[nv_courant] = precedentes[nv_courant]->suivante[nv_courant];
  precedentes[nv_courant]->suivante[nv_courant] = nlle;
  nv_courant++;
  while(nv_courant<=nv_max) {
    if(pile_ou_face()) {
      nlle->suivante.push_back(nullptr);
      nlle->suivante[nv_courant] = precedentes[nv_courant]->suivante[nv_courant];
      precedentes[nv_courant]->suivante[nv_courant] = nlle;
      nv_courant++;
    } else {
      return;
    }
  }
  while(pile_ou_face()) {
    m_sentinelle->suivante.push_back(nullptr);
    nlle->suivante.push_back(nullptr);
    m_sentinelle->suivante[nv_courant]=nlle;
    nv_courant++;
  }
}

SkipCellule* SkipListe::chercher(int v) {
  /* cette recherche ne fait rien, c'est votre travail de coder cette fonction */
  //return nullptr ;

  /* pour chercher dans une liste triee : avancer dans la liste tant qu'on est
   * sur une valeur strictement plus petite. Si on arrive sur une valeur égale,
   * on a trouve, si on arrive sur la fin de la liste ou une valeur plus grande,
   * on ne trouvera pas plus loin car la liste est (devrait etre) triee.
   *
   * initialiser un curseur sur la sentinelle
   * Tant que le curseut a uns cellule suivante et que sa valeur est plus petite que v {
   *   avancer le curseur sur sa suivante
   * }
   * Si le curseur n'est pas la sentinelle et a la valeur v
   *   le retourner
   * Sinon
   *   retourner nullptr
   *
   

  SkipCellule* cur = m_sentinelle;
  //std::cout << "Valeur recherchée: " << v << std::endl;

  if(m_sentinelle->suivante.size()==2) {
    while(cur->suivante[1] != nullptr && cur->suivante[1]->valeur < v) {
      //std::cout << "Niv 1 " << cur->valeur << std::endl;
      cur = cur->suivante[1];
    }
    //std::cout << "Sortie du while Niv 1" << std::endl;
  }
  while(cur->suivante[0] != nullptr && cur->suivante[0]->valeur < v) {
    //std::cout << "Niv 0 " << cur->suivante[0]->valeur << std::endl;
    cur = cur->suivante[0];
  }
  if(cur != m_sentinelle && cur->suivante[0] != nullptr && cur->suivante[0]->valeur == v) {
    //std::cout << "Valeur trouvée: " << cur->suivante[0]->valeur << std::endl;
    return cur->suivante[0];
  }
    //std::cout << "Valeur non trouvée: " << v << std::endl;
    return nullptr;
  */

  int nv_max = m_sentinelle->suivante.size()-1;
  int nv_courant = nv_max;
  SkipCellule* cur = m_sentinelle;
  while(nv_courant>=0) {
    while(cur->suivante[nv_courant] != nullptr && cur->suivante[nv_courant]->valeur < v) {
      cur = cur->suivante[nv_courant];
    }
    nv_courant--;
  }
  if(cur!=m_sentinelle && cur->suivante[0]!=nullptr && cur->suivante[0]->valeur == v) {
    return cur->suivante[0];
  } 
  return nullptr;


}

bool SkipListe::test_tri() {
  //initialisation du curseur
  SkipCellule* courante = m_sentinelle->suivante[0] ;
  while(courante && courante->suivante[0]) {
    //on a deux cellules de suite qui existent
    if(courante->valeur > courante->suivante[0]->valeur) {
      //la premiere a une valeur plus grande que la seconde, ce n'est pas trie
      return false ;
    }
    courante = courante->suivante[0] ;
  }
  return true ;
}

void SkipListe::afficher() {
  for(int i = 0; i<m_sentinelle->suivante.size(); i++) {
    std::cout << "[ " ;
    //parcours lineaire du niveau 0
    SkipCellule* courante = m_sentinelle->suivante[i] ;
    while(courante) {
      std::cout << courante->valeur << " " ;
      courante = courante->suivante[i] ;
    }
    std::cout << "]" << std::endl ;
  }
  
}

void SkipListe::ajouter_niveau() {
  int nv_max = m_sentinelle->suivante.size()-1;
  m_sentinelle->suivante.push_back(nullptr);
  SkipCellule* cur0 = m_sentinelle->suivante[nv_max];
  SkipCellule* prec = m_sentinelle;
  while(cur0->suivante[nv_max]) {
    if(pile_ou_face()) {
      cur0->suivante.push_back(nullptr);
      prec->suivante[nv_max+1] = cur0;
      prec = cur0;
    }
    cur0 = cur0->suivante[nv_max];
  }
}


bool SkipListe::pile_ou_face() {
  //lancer la piece
  return m_piece(m_random) ;
}
